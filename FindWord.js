const fs = require("fs");

const searchWords = (grid, words) => {
  let result = [];

  const search = (word, i, j, di, dj) => {
    let r = i,
      c = j;
    for (let k = 0; k < word.length; k++) {
      if (
        r >= grid.length ||
        r < 0 ||
        c < 0 ||
        c >= grid[0].length ||
        grid[r][c] !== word[k]
      )
        return false;

      r += di;
      c += dj;
    }
    return true;
  };

  const dirs = [
    [-1, 0],
    [-1, 1],
    [0, 1],
    [1, 1],
    [1, 0],
    [1, -1],
    [0, -1],
    [-1, -1],
  ];

  for (let i = 0; i < grid.length; i++) {
    for (let j = 0; j < grid[0].length; j++) {
      for (let k = 0; k < words.length; k++) {
        let word = words[k];
        for (let l = 0; l < dirs.length; l++) {
          let [di, dj] = dirs[l];

          if (search(word, i, j, di, dj)) {
            let endIndex_x = i + (word.length - 1) * di;
            let endIndex_y = j + (word.length - 1) * dj;
            result.push(
              word + " " + i + ":" + j + " " + endIndex_x + ":" + endIndex_y
            );
          }
        }
      }
    }
  }
  return result;
};

fs.readFile("input.txt", "utf-8", (err, data) => {
  if (err) throw err;

  let lines = data.split("\n");
  let [rowCol, ...grid] = lines.slice(0, -1);
  let [rows, cols] = rowCol.split("x").map(Number);
  let words = lines.slice(rows + 1);
  let grid2D = [];
  for (let i = 0; i < rows; i++) {
    grid2D.push(grid[i].split(" "));
  }

  let result = searchWords(grid2D, words);
  result.forEach((x) => console.log(x));
});
